const place = prompt("Name a place that you visited");
const day = prompt("When did you last go to that place");
const activities = prompt("The things that you love to do on weekends/holidays");

function play1() {
    let newElement = document.createElement("div");
    document.write("My Last Fun Getaway");
    newElement.textContent = JSON.stringify("When we have the time to play or relax together, we usually " + activities + ". " + "We usually do them on the weekend. " + "This time, we did it on " + day + ", we went to " + place + ".");
    document.body.appendChild(newElement);
}
play1();